package com.backend.projeto.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String email;

    private String password;

    public User() {
    }

    public User( String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }
}
